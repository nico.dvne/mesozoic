﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using Mesozoic;

namespace mesozoictest
{
    public class HordeTest
    {
        [TestMethod]
        public void TestAddDinosaur()
        {
            Horde horde = new Horde();
            Stegausaurus louis = new Stegausaurus("Louis", 10);
            
            //Assert.IsEmpty(horde.GetDinosaurs());
            horde.AddDinosaur(louis);
            Assert.AreEqual(1,horde.GetDinosaurs());
            Assert.AreEqual(louis, horde.GetDinosaurs()[0]);
            
        }

        [TestMethod]
        public void TestRemoveDinosaur()
        {
            Horde horde = new Horde();
            TRex louis = new TRex("Louis", 12);
            Stegausaurus nessie = new Stegausaurus("Nessie", 11);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            Assert.AreEqual(2, horde.GetDinosaurs().Count);
            horde.RemoveDinosaur(louis);
            Assert.AreEqual(1,horde.GetDinosaurs());
            horde.RemoveDinosaur(nessie);
            Assert.IsNull(horde.GetDinosaurs());
        }


        [TestMethod]
        public void TestIntroduceAll()
        {
            Horde horde = new Horde();
            Stegausaurus louis = new Stegausaurus("Louis", 12);
            Diplodocus nessie = new Diplodocus("Nessie", 11);
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);

            string expected_introduction = "Je suis Louis le Stegausaurus, j'ai 12 ans.\nJe suis Nessie le Diplodocus, j'ai 11 ans.\n";

            Assert.AreEqual(expected_introduction, horde.IntroduceAll());
        }
    }
}