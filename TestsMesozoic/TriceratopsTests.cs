﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace TestsMesozoic
{
    [TestClass]
    public class TriceratopsTests
    {
        [TestMethod]
        public void TestConstructor()
        {
            Triceratops louis = new Triceratops("Louis", 15);
            Assert.IsNotNull(louis);
            Assert.AreEqual("Louis", louis.GetName());
            Assert.AreEqual("Triceratops", louis.GetSpecie());
            Assert.AreEqual(15, louis.GetAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Triceratops louis = new Triceratops("Louis", 15);
            Assert.AreEqual("Grrr", louis.Roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Triceratops louis = new Triceratops("Louis", 15);
            Assert.AreEqual("Je suis Louis le Triceratops, j'ai 15 ans.", louis.SayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Triceratops louis = new Triceratops("Louis", 15);
            Triceratops marine = new Triceratops("Marine", 15);
            Assert.AreEqual("Je suis Louis et je ne peux pas me faire de câlin à moi-même :'(.", louis.Hug(louis));
            Assert.AreEqual("Je suis Louis et je fais un câlin à Marine.", louis.Hug(marine));
        }
        [TestMethod]
        public void TestToString()
        {
            Dinosaur louis = new Diplodocus("Louis", 15);
            {
                Assert.AreEqual("Mesozoic.Diplodocus = { name: Louis, age: 15}", louis.ToString());
            }
        }
    }
}
