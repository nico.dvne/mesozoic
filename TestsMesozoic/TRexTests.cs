﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace TestsMesozoic
{
    [TestClass]
    public class TRexTests
    {
        [TestMethod]
        public void TestConstructor()
        {
            TRex louis = new TRex("Louis", 15);
            Assert.IsNotNull(louis);
            Assert.AreEqual("Louis", louis.GetName());
            Assert.AreEqual("TRex", louis.GetSpecie());
            Assert.AreEqual(15, louis.GetAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            TRex louis = new TRex("Louis", 15);
            Assert.AreEqual("Grrr", louis.Roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            TRex louis = new TRex("Louis", 15);
            Assert.AreEqual("Je suis Louis le TRex, j'ai 15 ans.", louis.SayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            TRex louis = new TRex("Louis", 15);
            TRex marine = new TRex("Marine", 15);
            Assert.AreEqual("Je suis Louis et je ne peux pas me faire de câlin à moi-même :'(.", louis.Hug(louis));
            Assert.AreEqual("Je suis Louis et je fais un câlin à Marine.", louis.Hug(marine));
        }
        [TestMethod]
        public void TestToString()
        {
            Dinosaur louis = new Diplodocus("Louis", 15);
            {
                Assert.AreEqual("Mesozoic.Diplodocus = { name: Louis, age: 15}", louis.ToString());
            }
        }
    }

}
