﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;
namespace TestsMesozoic
{
    [TestClass]
    public class DiplodocusTest
    {
        [TestMethod]
        public void TestConstructor()
        {
            Diplodocus louis = new Diplodocus("Louis", 15);
            Assert.IsNotNull(louis);
            Assert.AreEqual("Louis", louis.GetName());
            Assert.AreEqual("Diplodocus", louis.GetSpecie());
            Assert.AreEqual(15, louis.GetAge());
        }
        [TestMethod]
        public void TestRoar()
        {
            Diplodocus louis = new Diplodocus("Louis", 15);
            Assert.AreEqual("Grrr", louis.Roar());
        }
        [TestMethod]
        public void TestSayHello()
        {
            Diplodocus louis = new Diplodocus("Louis", 15);
            Assert.AreEqual("Je suis Louis le Diplodocus, j'ai 15 ans.", louis.SayHello());
        }
        [TestMethod]
        public void TestHug()
        {
            Diplodocus louis = new Diplodocus("Louis", 15);
            Diplodocus marine = new Diplodocus("Marine", 15);
            Assert.AreEqual("Je suis Louis et je ne peux pas me faire de câlin à moi-même :'(.", louis.Hug(louis));
            Assert.AreEqual("Je suis Louis et je fais un câlin à Marine.", louis.Hug(marine));
        }
        [TestMethod]
        public void TestToString()
        {
            Dinosaur louis = new Diplodocus("Louis", 15);
            {
                Assert.AreEqual("Mesozoic.Diplodocus = { name: Louis, age: 15}", louis.ToString());
            }
        }
        
        
       
    }
}
