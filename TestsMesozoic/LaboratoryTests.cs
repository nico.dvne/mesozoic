﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Mesozoic;

namespace Mesozoic
{
    [TestClass]
    public class LaboratoryTests
    {
        /*
        [TestMethod]
        public void TestCreateDiplodocus()
        {
            Diplodocus louis = Laboratory.createDiplodocus("Louis");
            Assert.IsNotNull(louis);
            Assert.AreEqual("Diplodocus", louis.GetSpecie());
            Assert.AreEqual("Louis", louis.GetName());
        }
        public void TestCreateStegausaurus()
        {
            Stegausaurus alex = Laboratory.createStegausaurus("alex");
            Assert.IsNotNull(alex);
            Assert.AreEqual("Stegausaurus", alex.GetSpecie());
            Assert.AreEqual("alex", alex.GetName());
            Assert.AreEqual(0, alex.GetAge());
        }
        public void TestCreateTrex()
        {
            TRex alex = Laboratory.createTRex("alex");
            Assert.IsNotNull(alex);
            Assert.AreEqual("TRex", alex.GetSpecie());
            Assert.AreEqual("alex", alex.GetName());
            Assert.AreEqual(0, alex.GetAge());
        }
        public void TestCreateTriceratops()
        {
            Triceratops alex = Laboratory.createTriceratops("alex");
            Assert.IsNotNull(alex);
            Assert.AreEqual("Triceratops", alex.GetSpecie());
            Assert.AreEqual("alex", alex.GetName());
            Assert.AreEqual(0, alex.GetAge());
        }*/
        public void TestLaboCreate()
        {
            Diplodocus Leon = Laboratory.CreateDinosaur<Diplodocus>("Leon", 19);
            Assert.AreEqual("Leon", Leon.GetName());
            Assert.AreEqual(19, Leon.GetAge());
        }
    }
}
