﻿using System;

namespace Mesozoic
{
    public abstract class Dinosaur //abstact veut dire que c'est la classe mère.
    {
        protected string name;
        protected string specie;
        protected int age;

        public Dinosaur(string name, int age, string specie) //constructeur
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }

        public string SayHello()
        {
            return String.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, this.specie, this.age);
        }

        public string Roar()
        {
            return "Grrr";
        }


        public string Hug <TDinosaur> (TDinosaur dinosaur) where TDinosaur: Dinosaur
            /*Veut dire, je vais te passer un argument, il doit être de type dinosaur*/
        {
            if (dinosaur == this)
            {
                return String.Format("Je suis {0} et je ne peux pas me faire de câlin à moi-même :'(.", this.name);
            }
            return String.Format("Je suis {0} et je fais un câlin à {1}.", this.name, dinosaur.GetName());
        }


        public string GetName()
        {
            return this.name;
        }

        public string GetSpecie()
        {
            return this.specie;
        }

        public int GetAge()
        {
            return this.age;
        }

        public void SetName(string name)
        {
            this.name = name;
        }

        public void SetSpecie(string specie)
        {
           this.specie = specie;
        }

        public void SetAge(int age)
        {
            this.age = age;
        }
        
        public override string ToString()
        {
            
            string coordonnes;           
            coordonnes = String.Format(" name: {0}, age: {1}" ,this.name, this.age);
            string phrase_globale = String.Format(base.ToString() +" = "+"{{"+  coordonnes + "}}" );
            return phrase_globale; 
        }
        public override int GetHashCode()
        {
            int hash = 13;
            hash ^= this.name.GetHashCode();
            hash ^= this.age.GetHashCode();
            
            return hash;
        }
        //public override bool Equals(object obj)
        //{
        //    if (this.GetHashCode() == obj.GetHashCode())
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }


        //}
    }        
}