﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic; 

namespace Mesozoic
{
    public class Diplodocus : Dinosaur
    {
        public Diplodocus(string name, int age) : base(name, age,"Diplodocus")
        {
            this.name = name;            
            this.age = age;
        }
       
    }
}
