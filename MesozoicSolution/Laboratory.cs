﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Mesozoic
{
    public class Laboratory
    {
        public static TDinosaur CreateDinosaur<TDinosaur>(string name, int age = 0) where TDinosaur : Dinosaur
        {
            return (TDinosaur)Activator.CreateInstance(typeof(TDinosaur), name, age);
        }

        /* public static Diplodocus createDiplodocus(string name)
          {
              return new Diplodocus(name, 0); //retourne un dinosaur avec son nom ( le paramètre ) et son age : 0 car il vient de naitre
          }
          public static Stegausaurus createStegausaurus(string name)
          {
              return new Stegausaurus(name, 0);
          }
          public static TRex createTRex(string name)
          {
              return new TRex(name, 0);
          }
          public static Triceratops createTriceratops(string name)
          {
              return new Triceratops(name, 0);
          }
          */
    }
}
