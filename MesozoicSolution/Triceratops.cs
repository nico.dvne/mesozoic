﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Mesozoic
{
    public class Triceratops : Dinosaur
    {
        public Triceratops(string name, int age) : base(name, age,"Triceratops")
        {
            this.name = name;
            
            this.age = age;
        }    
                                    
    }
}
