﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Mesozoic
{
    public class TRex : Dinosaur
    {
        public TRex(string name, int age) : base(name, age,"TRex")
        {
            this.name = name;
            
            this.age = age;
        }
    }
}
