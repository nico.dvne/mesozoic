﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;

namespace Mesozoic
{
    public class Stegausaurus : Dinosaur
    {
        public Stegausaurus(string name, int age): base(name,age, "Stegausaurus")
        {
            this.name = name;
            this.age = age;
        }
    }
}
